# Adiuto Website

## Requirements

1. docker and docker-compose

[comment]: <> (2. rsync v3.x)

[comment]: <> (   !!! MacOS &#40;up to v10.14.4&#41; is shipped with rsync 2.x. The option "--iconv" which is used in "fetch_content.sh" is only available in rsync 3.x. It's possible to install rsync 3.x via homebrew or macport.)

## Project startup

```bash
# Set port bindings for docker
cp docker-compose.override.sample.yml docker-compose.override.yml

# Build docker images
docker-compose -f docker-compose.build.yml build
docker-compose build

# Run build container to install dependencies
docker-compose -f docker-compose.build.yml run --rm build /app/bin/install-dependencies.sh

# Run build container to build the project
docker-compose -f docker-compose.build.yml run --rm build /app/bin/build.sh
```

### Performance
If you have performance issues while using a Mac, adjust your system configuration and docker-composer.override.yaml file using NFS.

### Start containers
```bash
# Sync content from remote server
#SOURCE_HOST=<SSH-USERNAME>@domain SOURCE_PATH=domain/current bin/fetch_content.sh

# Start containers. This may take a while, as the db needs to be imported
docker-compose up -d

# Follow logs to see the containers run up
docker-compose logs -f web db
```

## Reset

Kill all containers (looses all persistency, but mounted volumes)
```bash
# Down all running containers
docker-compose down -v
docker-compose -f docker-compose.build.yml down -v

# Rebuild docker images
docker-compose build
docker-compose -f docker-compose.build.yml build

# Rebuild project
docker-compose -f docker-compose.build.yml run --rm build

# Restart containers (webserver, solr, redis)
docker-compose up -d
```

## Access Composer via bash

```bash
# Start bash inside the build container
docker-compose -f docker-compose.build.yml run build bash

# Verify Composer is available
composer -V
```
