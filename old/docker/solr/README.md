Dockerfile for a TYPO3 solr server

The Dockerfile is a copy of the Dockerfile from the solr extension: `app/web/typo3conf/ext/solr/Dockerfile`
The solr folder is also copied from the original extension: `app/web/typo3conf/ext/solr/Resources/Private/Solr`, but most cores (except de, it, fr and en) have been removed.  

We need this copy, as the original files will not have been installed, when the composer setup is first run. 
