Generate new ssl certificates

    openssl req -x509 -nodes -days 1460 -sha256 -newkey rsa:2048 \
        -subj '/CN=adiuto.org.develop' \
        -reqexts SAN -extensions SAN \
        -config <(cat /etc/ssl/openssl.cnf \
            <(printf "\n[SAN]\nsubjectAltName=DNS:adiuto.org.develop,DNS:*.adiuto.org.develop")) \
        -keyout ./certs/cert.key \
        -out ./certs/cert.pem

