#!/bin/bash

while ! mysqladmin ping -h"$DRUPAL_DB_HOST" --silent; do
    echo "Waiting for MariaDB server to become available..."
    sleep 1
done

echo "Creating drupal DB...";
mysql -uroot -p${MYSQL_ROOT_PASSWORD} -h${DRUPAL_DB_HOST} \
      -e "CREATE DATABASE ${DRUPAL_DB_NAME} CHARACTER SET utf8 COLLATE utf8_general_ci";

echo "Creating drupal DB user...";
mysql -uroot -p${MYSQL_ROOT_PASSWORD} -h${DRUPAL_DB_HOST} \
      -e "CREATE USER '${DRUPAL_DB_USER}'@'%' IDENTIFIED BY '${DRUPAL_DB_PASS}'";

echo "Setting drupal DB user permissions...";
mysql -uroot -p${MYSQL_ROOT_PASSWORD} -h${DRUPAL_DB_HOST} \
      -e "GRANT LOCK TABLES, SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON ${DRUPAL_DB_NAME}.* TO '${DRUPAL_DB_USER}'@'%' IDENTIFIED BY '${DRUPAL_DB_PASS}'";

# Install dependencies
mv /tmp/composer.json /var/www/html;
pushd /var/www/html || true;
composer install --dev;
popd || true;

# Make drush available as a command
ln -s /var/www/html/vendor/bin/drush /usr/local/bin/drush;

# Install Drupal
pushd /var/www/html || true;
drush dl drupal-${DRUPAL_VERSION};
# Symlink drupal
ln -s drupal-${DRUPAL_VERSION}/ public;
# Install default site
pushd /var/www/html/public || true;
drush si --db-url=mysql://${DRUPAL_DB_USER}:${DRUPAL_DB_PASS}@${DRUPAL_DB_HOST}/${DRUPAL_DB_NAME} --yes;
popd || true;

# Get custom modules
pushd /var/www/html/public/sites/all/modules || true;
git clone https://gitlab.com/adiuto/adiuto.org.git custom;
popd || true;
# Get custom libraries
pushd /var/www/html/public/sites/all || true;
rm -rf /var/www/html/public/sites/all/libraries;
git clone https://gitlab.com/adiuto/adiuto-libraries.git libraries;
popd || true;
# Get custom themes
pushd /var/www/html/public/sites/all/themes || true;
git clone https://gitlab.com/adiuto/adiuto-themes.git;
mv /var/www/html/public/sites/all/themes/adiuto-themes/* /var/www/html/public/sites/all/themes;
popd || true;

# Install additional modules
pushd /var/www/html/public || true;
echo "Downloading additional modules..."
drush dl ${DRUPAL_ADDITIONAL_MODULES} --yes;
echo "Disabling superfluous modules..."
for module_identifier in ${DRUPAL_DISABLED_MODULES//,/ };
do
  drush dis $module_identifier --yes;
done
echo "Enabling required modules..."
for module_identifier in ${DRUPAL_ENABLED_MODULES//,/ };
do
  drush en $module_identifier --yes;
done
popd || true;

# Fix privatemsg module
pushd /var/www/html/public/sites/all/modules || true;
rm -rf privatemsg
git clone https://git.drupalcode.org/project/privatemsg.git
popd || true;

# Import DB fixture
echo "Importing DB fixture. This could take a while..."
zcat /tmp/adiutosql1.sql.gz | mysql -u${DRUPAL_DB_USER} -p${DRUPAL_DB_PASS} -h${DRUPAL_DB_HOST} ${DRUPAL_DB_NAME};

# Setup default admin
pushd /var/www/html/public || true;
drush user-create adiutoadmin --password=password
drush user-add-role "administrator" adiutoadmin
popd || true;

# Fix permissions
pushd /var/www/html/public || true;
chown -R www-data:www-data ./*
chown -R www-data:www-data ./.*
popd || true;

# Start apache
apache2-foreground
